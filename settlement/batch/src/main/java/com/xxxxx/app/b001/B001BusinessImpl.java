package com.xxxxx.app.b001;

import com.xxxxx.core.app.up.up001.business.TestTableBusiness;
import com.xxxxx.core.entity.TestTable;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class B001BusinessImpl implements B001Business {

    @Autowired
    TestTableBusiness business;

    public B001BusinessImpl() {
        // TODO 自動生成されたコンストラクター・スタブ
    }

    /**
     */
    public void getData() {

        log.debug("----> " + this.getClass().getSimpleName());
        List<TestTable> dataList = business.findAll();
        dataList.forEach(data -> {
            log.info(String.valueOf(data.getC1()) + "|" + data.getC2() + "|" + data.getC3());
        });
        log.debug("<---- " + this.getClass().getSimpleName());
    }

}
