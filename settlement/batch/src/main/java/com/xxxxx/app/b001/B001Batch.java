package com.xxxxx.app.b001;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author azs
 */
@Slf4j
public class B001Batch implements ApplicationRunner {

    @Autowired
    private B001Business business;

    /**
     * B001のメイン。
     * @param args バッチ起動引数
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.debug("----> " + this.getClass().getSimpleName());
        business.getData();
        log.debug("<---- " + this.getClass().getSimpleName());
    }

    @Configuration
    public static class B001BatchConfig {
        @Bean
        @ConditionalOnProperty(value = { "batch.execute" }, havingValue = "B001")
        public B001Batch b001BatchRunner() {
            return new B001Batch();
        }
    }
}
