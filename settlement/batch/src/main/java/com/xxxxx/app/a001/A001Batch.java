package com.xxxxx.app.a001;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author azs
 */
@Slf4j
public class A001Batch implements ApplicationRunner {

    /**
     * A001のメイン。
     * @param args バッチ起動引数
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info("----> " + this.getClass().getSimpleName());

        Long milli = System.currentTimeMillis();
        log.debug("currentTimeMillis --> " + String.valueOf(milli));
        // long→Instant
        Instant ins = Instant.ofEpochMilli(milli);
        // Instant→文字列
        String str1 = DateTimeFormatter.ISO_INSTANT.format(ins);
        log.debug("ISO_INSTANT --> " + str1);
        String str2 = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS").format(LocalDateTime.ofInstant(ins, ZoneId
                .systemDefault()));
        log.debug("ofPattern --> " + str2);

        long unixTimestamp = Instant.now().getEpochSecond();
        log.debug("getEpochSecond --> " + String.valueOf(unixTimestamp));
        // long→Instant
        Instant ins2 = Instant.ofEpochMilli(unixTimestamp);
        // Instant→文字列
        String str3 = DateTimeFormatter.ISO_INSTANT.format(ins2);
        log.debug("ISO_INSTANT --> " + str3);

        log.info("<---- " + this.getClass().getSimpleName());
    }

    @Configuration
    public static class A001BatchConfig {
        @Bean
        @ConditionalOnProperty(value = { "batch.execute" }, havingValue = "A001")
        public A001Batch a001BatchRunner() {
            return new A001Batch();
        }
    }
}
