package com.xxxxx.fw;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfiguration {

    @Bean
    public AppContext configApp() {
        return new AppContext();
    }
}
