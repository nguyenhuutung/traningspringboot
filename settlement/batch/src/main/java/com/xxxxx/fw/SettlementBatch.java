package com.xxxxx.fw;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.util.StringUtils;

/**
 * C決済バッチメインクラス.
 * @author azs
 */
@SpringBootApplication(scanBasePackages = { "com.xxxxx.app", "com.xxxxx.fw", "com.xxxxx.core" })
@EnableJpaRepositories("com.xxxxx.core.repository")
@EntityScan("com.xxxxx.core.entity")
@Slf4j
public class SettlementBatch {

    @Autowired
    private AppContext appContext;

    /**
     * C決済バッチメインメソッド.
     * @param args 起動対象のクラス名
     */
    public static void main(String[] args) {

        log.info(">>>>> EntryPoint Start");
        SpringApplication.run(SettlementBatch.class, args);
//        try {
//            ConfigurableApplicationContext ctx = SpringApplication.run(SettlementBatch.class, args);
//            SettlementBatch cls = ctx.getBean(SettlementBatch.class);
//            cls.execute(args);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        log.info("<<<<< EntryPoint End");
    }

    /**
     * @param args
     * @throws Exception
     */
    public void execute(String[] args) throws Exception {

        String[] sendArgs = new String[args.length - 1];
        for (int i = 0; i < sendArgs.length; i++) {
            sendArgs[i] = args[i + 1];
        }

        String path = appContext.getPath();
        if (StringUtils.isEmpty(path)) {
            throw new Exception("path is null!!");
        }

        String className = path + args[0];

        log.info("--->>  Execute " + className);
        try {
            Class<?> main = Class.forName(className);
            Object[] vargs = { sendArgs };
            main.getMethod("main", new Class[] { String[].class }).invoke(null, vargs);
            // TODO 処理結果のもどし
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("<<--- Execute " + className);
    }
}
