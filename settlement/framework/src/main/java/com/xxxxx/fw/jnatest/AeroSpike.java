package com.xxxxx.fw.jnatest;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;

interface LibOpen extends Library {

	// loadLibraryの第一引数はあとで作成するlib***.soの***と一致させる。
	LibOpen INSTANCE = (LibOpen) Native.loadLibrary("SDBLibrary", LibOpen.class);

	// Cの関数名と一致させる
	int SDB_OpenLibrary(String ip);

//	int SDB_OpenLibraryWithHostID(String ip, long hostId);

	int SDB_CloseLibrary();

	String SDB_GetLibraryVersionString();

//	int SDB_ReadByteArrayRecord2(String tableNum, String key, String keySize, String[] array, int size);
//	int SDB_ReadByteArrayRecord2(byte tableNum, byte key[], byte keySize, ByteBuffer array, IntBuffer size);
//	int SDB_ReadByteArrayRecord2(byte tableNum, byte key[], byte keySize, Pointer array, IntByReference size);
	int SDB_ReadByteArrayRecord2(byte tableNum, byte key[], byte keySize, ByteBuffer array, IntByReference size);

	int SDB_WriteByteArrayRecord2(byte tableNum, byte key[], byte keySize, byte array[], int size);

}

public class AeroSpike {

	public enum tables {
		table1, table2, table3
	}

	public int open() {

		final String ipAddr = "192.168.100.135";

		LibOpen lib = LibOpen.INSTANCE;
		return lib.SDB_OpenLibrary(ipAddr);
	}

//	public int openWithHostID() {
//
//		final String ipAddr = "192.168.100.135";
////		final String ipAddr = "192.168.100.135:8081";
//		final long hostId = Long.parseLong("007f0100", 16);
////		final long hostId = Long.parseLong("a8c08764", 16);
//
//		LibOpen lib = LibOpen.INSTANCE;
//		return lib.SDB_OpenLibraryWithHostID(ipAddr, hostId);
//	}

	public String getVersion() {

		LibOpen lib = LibOpen.INSTANCE;
		return lib.SDB_GetLibraryVersionString();
	}

	/**
	 * 
	 * @param pTableNum
	 * @param pKey
	 * @return
	 */
	public String read(tables pTableNum, String pKey) throws Exception {

		LibOpen lib = LibOpen.INSTANCE;
		byte tableNum = Byte.parseByte(String.valueOf(pTableNum.ordinal()));
		byte key[] = pKey.getBytes();
		byte keySize = Byte.parseByte(String.valueOf(pKey.length()));
//		byte array[]=new byte[1024000];
		ByteBuffer bb = ByteBuffer.allocate(1024000);
		// 初期値:最大1MB
		int size = 1024000;
//		IntBuffer iBuf = IntBuffer.allocate(size);
//		int result = lib.SDB_ReadByteArrayRecord2(Byte.parseByte(tableNum), key.getBytes(), Byte.parseByte(keySize),
//				buf, iBuf);

		IntByReference iSize = new IntByReference(size);
		int result = lib.SDB_ReadByteArrayRecord2(tableNum, key, keySize, bb, iSize);
		// array = pointer.getStringArray(0);

		// TODO resultの値によるエラー判定
		if (result != 0) {
			throw new Exception(String.valueOf(result));
		}

		String value = StandardCharsets.UTF_8.decode(bb).toString();
		return value;
	}

	/**
	 * 
	 * @param pTableNum
	 * @param pKey
	 * @param pAryValue
	 * @return
	 */
	public int write(tables pTableNum, String pKey, String[] pAryValue) {

		LibOpen lib = LibOpen.INSTANCE;
		byte tableNum = Byte.parseByte(String.valueOf(pTableNum.ordinal()));
		byte key[] = pKey.getBytes();
		byte keySize = Byte.parseByte(String.valueOf(pKey.length()));
		byte array[] = new byte[1024000];
		// 初期値:0(最大1MB)
		int size = 0;

		// binの編集
		for (int i = 0; i < pAryValue.length; i++) {
			for (int j = 0; j < pAryValue[i].getBytes().length; j++) {
				// つづきから編集する
				array[j + size] = pAryValue[i].getBytes()[j];
			}
//			array[i]=Byte.parseByte(pValue[i]);
			size += pAryValue[i].getBytes().length;
		}

		int result = lib.SDB_WriteByteArrayRecord2(tableNum, key, keySize, array, size);
		return result;
	}

}
