package com.xxxxx.fw.common;

import java.io.BufferedInputStream;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class AwsConnect {

//	public void connection() {
	public Session connection() {

		// サーバ
		String sshHost = "ec2-54-178-186-213.ap-northeast-1.compute.amazonaws.com";
		// ポート
		int sshPort = 22;
		// ユーザ
		String sshUser = "azs01";
//		// パスワード
//		String password = "パスワード";
//		// コマンド
//		String command = "date";

		// sshトンネル設定
		String remoteHost = "127.0.0.1";
		int remotePort = 5432;
		int localPort = 12345;

		// 鍵
		String identityKeyFileName = "key/quadrac-psp.pem";

		JSch jsch;
		Session session = null;
//		ChannelExec channel = null;
//		BufferedInputStream bin = null;

		try {
			jsch = new JSch();
			// 鍵追加
			jsch.addIdentity(Thread.currentThread().getContextClassLoader().getResource(identityKeyFileName).getFile());
			// Session設定
			session = jsch.getSession(sshUser, sshHost, sshPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();
			session.setPortForwardingL(localPort, remoteHost, remotePort);
			// // 接続
//			channel.connect();
			
		} catch (JSchException jse) {
			jse.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();

//		} finally {
//			if (channel != null) {
//				try {
//					channel.disconnect();
//				} catch (Exception e) {
//				}
//			}
//			if (session != null) {
//				try {
//					session.disconnect();
//				} catch (Exception e) {
//				}
//			}
		}
		return session;
	}

}
