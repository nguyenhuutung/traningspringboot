package com.xxxxx.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication(scanBasePackages = { "com.xxxxx.app", "com.xxxxx.core" })
@EnableJpaRepositories("com.xxxxx.core.repository")
@EntityScan("com.xxxxx.core.entity")
@Slf4j
public class PortalApplication {

    public static void main(String[] args) {
        log.debug("start --->>");
        SpringApplication.run(PortalApplication.class, args);
        log.debug("<<--- end");
    }
}
