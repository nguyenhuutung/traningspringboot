package com.xxxxx.app.up.up002.controller;

import com.xxxxx.app.up.up002.model.AeroSpikeModel;
import com.xxxxx.fw.jnatest.AeroSpike;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
// @RestController
@Controller
// @RequestMapping("/")
public class AeroSpikeController {

    // @Autowired
    // TestTableService service;

    @RequestMapping(value = "/open", method = RequestMethod.GET)
    // public List<TestTable> index() {
    // return service.findAll();
    // }
    public ModelAndView index(ModelAndView mov) {

        AeroSpikeModel model = new AeroSpikeModel();
        model.setMsg("plz click button");

        mov.setViewName("aerospike");
        mov.addObject("model", model);
        return mov;
    }

    @RequestMapping(value = "/open", method = RequestMethod.POST)
    public ModelAndView send(ModelAndView mov) {

        AeroSpike jna = new AeroSpike();
        int result = jna.open();
        log.debug("open : " + result);
        // int result = jna.openWithHostID();
        // String result = jna.getVersion();
        String value = "";

        if (result == 0) {
            // String tableNum = "1";
            String key = "1";

            try {
                value = jna.read(AeroSpike.tables.table1, key);

            } catch (Exception e) {
                result = Integer.parseInt(e.getMessage());
                log.debug("read : " + e.getMessage());
            }
        }

        AeroSpikeModel model = new AeroSpikeModel();
        model.setMsg("click button(reload)");
        model.setRtnMsg("result=" + String.valueOf(result) + "/value:" + value);

        mov.setViewName("aerospike");
        mov.addObject("model", model);
        return mov;
    }

    @RequestMapping(value = "/write", method = RequestMethod.POST)
    public ModelAndView write(ModelAndView mov) {

        AeroSpike jna = new AeroSpike();
        int result = jna.open();
        log.debug("open : " + result);

        if (result == 0) {
            String key = "1";
            String[] aryData = { "value1", "value2" };

            result = jna.write(AeroSpike.tables.table1, key, aryData);
            log.debug("write : " + result);
        }

        AeroSpikeModel model = new AeroSpikeModel();
        model.setMsg("click button(reload)");
        model.setRtnMsg("result=" + String.valueOf(result));

        mov.setViewName("aerospike");
        mov.addObject("model", model);
        return mov;
    }
}
