package com.xxxxx.app.up.up001.controller;

import com.xxxxx.app.up.up001.model.DemoModel;
import com.xxxxx.core.app.up.up001.business.TestTableBusiness;
import com.xxxxx.core.entity.TestTable;

import java.util.List;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

//@RestController
@Controller
// @RequestMapping("/")
@Slf4j
public class TestTableController {

    @Autowired
    // TestTableService service;
    TestTableBusiness business;

    @Autowired
    MessageSource msg;

    /**
     * @param mov
     * @return
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    // public List<TestTable> index() {
    // return service.findAll();
    // }
    public ModelAndView index(ModelAndView mov) {
        // Locale locale = Locale.getDefault();
        Locale locale = LocaleContextHolder.getLocale();
        log.debug("country: " + locale.getDisplayCountry());
        log.debug("language: " + locale.getDisplayLanguage());

        // LocaleContextHolderに画面で選択したロケールが保持されている
        log.info(this.msg.getMessage("hello", null, LocaleContextHolder.getLocale()));

        DemoModel model = new DemoModel();
        model.setMsg("plz click button");
        model.setDataList(null);

        mov.setViewName("demo");
        mov.addObject("model", model);
        return mov;
    }

    /**
     * @param mov
     * @param model
     * @param result
     * @return
     */
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public ModelAndView send(ModelAndView mov, @ModelAttribute("model") @Validated DemoModel model,
            BindingResult result) {

        // DemoModel model = new DemoModel();
        mov.setViewName("demo");

        if (result.hasErrors()) {
            result.getFieldErrors().forEach(err -> log.debug("err : " + err.getCode()));
            mov.getModel().putAll(result.getModel());

        } else {
            List<TestTable> dataList = business.findAll();

            model.setMsg("click button(reload)");
            model.setRtnMsg(msg.getMessage("result_msg", null, LocaleContextHolder.getLocale()));
            model.setDataList(dataList);
            mov.addObject("model", model);
        }

        return mov;
    }

    // private void aaa() {
    //
    // BlockingDeque<TestTable> deque = new LinkedBlockingDeque<TestTable>();
    // deque.addAll(service.findAll());
    // // deque.addFirst(service.findAll());
    //
    // }
}
