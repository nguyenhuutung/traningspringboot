package com.xxxxx.app.up.up002.model;

public class AeroSpikeModel {

	private String msg;

	private String rtnMsg;

//	private List<TestTable> dataList;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getRtnMsg() {
		return rtnMsg;
	}

	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}

//	public List<TestTable> getDataList() {
//		return dataList;
//	}
//
//	public void setDataList(List<TestTable> dataList) {
//		this.dataList = dataList;
//	}
}
