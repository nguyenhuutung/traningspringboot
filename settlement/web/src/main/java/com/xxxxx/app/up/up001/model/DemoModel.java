package com.xxxxx.app.up.up001.model;

import com.xxxxx.core.entity.TestTable;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class DemoModel {

    @Getter
    @Setter
    private String msg;

    @Getter
    @Setter
    private String rtnMsg;

    @Getter
    @Setter
    private List<TestTable> dataList;

    @Getter
    @Setter
    @NotBlank(message = "{must_input}")
    @Size(max = 5)
    private String name;

    @Getter
    @Setter
    @Pattern(regexp = "[0-9]*")
    private String age;

}
