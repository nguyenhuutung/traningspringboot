package com.xxxxx.core.app.up.up001.business;

import com.xxxxx.core.entity.TestTable;
import com.xxxxx.core.repository.TestTableRepository;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Component
@Slf4j
public class TestTableBusinessImpl implements TestTableBusiness {

    @Autowired
    TestTableRepository repository;

    public List<TestTable> findAll() {
        log.debug("<---> " + this.getClass().getSimpleName());
        return repository.findAll();
    }

    // public List<TestTable> findAll() {
    // Session session = null;
    // try {
    // AwsConnect aws = new AwsConnect();
    // session = aws.connection();
    // return repository.findAll();
    //
    // } finally {
    // if (session != null) {
    // session.disconnect();
    // }
    // }
    // }
}
