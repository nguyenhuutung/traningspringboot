package com.xxxxx.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.xxxxx.core.entity.TestTable;

@Repository
public interface TestTableRepository extends JpaRepository<TestTable, Integer> {
}