package com.xxxxx.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xxxxx.core.entity.TestTable;
import com.xxxxx.core.repository.TestTableRepository;

@Service
@Transactional
@Component
public class TestTableService {

    @Autowired
    TestTableRepository repository;

    public List<TestTable> findAll() {
        return repository.findAll();
    }

    // public List<TestTable> findAll() {
    // Session session = null;
    // try {
    // AwsConnect aws = new AwsConnect();
    // session = aws.connection();
    // return repository.findAll();
    //
    // } finally {
    // if (session != null) {
    // session.disconnect();
    // }
    // }
    // }
}
