package com.xxxxx.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the "test_table" database table.
 * 
 */
@Entity
//@Table(schema="azsdb01", name="\"test_table\"")
@Table( name="\"test_table\"")
@NamedQuery(name="TestTable.findAll", query="SELECT t FROM TestTable t")
public class TestTable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"c1\"")
	private int c1;

	@Column(name="\"c2\"")
	private String c2;

	@Temporal(TemporalType.DATE)
	@Column(name="\"c3\"")
	private Date c3;

	public TestTable() {
	}

	public int getC1() {
		return this.c1;
	}

	public void setC1(int c1) {
		this.c1 = c1;
	}

	public String getC2() {
		return this.c2;
	}

	public void setC2(String c2) {
		this.c2 = c2;
	}

	public Date getC3() {
		return this.c3;
	}

	public void setC3(Date c3) {
		this.c3 = c3;
	}

}